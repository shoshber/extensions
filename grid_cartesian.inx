<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Cartesian Grid</_name>
  <id>grid.cartesian</id>
  <dependency type="executable" location="extensions">grid_cartesian.py</dependency>

  <param name="border_th"       type="float"   min="0" max="1000" _gui-text="Border Thickness:">1</param>
  <param name="border_th_unit"  type="enum"                       _gui-text="Border Thickness Unit:">
    <item value="px">px</item>
    <item value="mm">mm</item>
    <item value="cm">cm</item>
    <item value="mm">in</item>
  </param>
  <param name="tab" type="notebook">
    <page name="x_tab" _gui-text="X Axis">
      <param name="x_divs"          type="int"     min="1" max="1000" _gui-text="Major X Divisions:">6</param>
      <param name="dx"              type="float"   min="1" max="1000" _gui-text="Major X Division Spacing:">5</param>
      <param name="dx_unit"         type="enum"                       _gui-text="Major X Division Spacing Unit:">
        <item value="mm">mm</item>
        <item value="cm">cm</item>
        <item value="in">in</item>
        <item value="px">px</item>
      </param>
      <param name="x_subdivs"       type="int"     min="1" max="1000" _gui-text="Subdivisions per Major X Division:">1</param>
      <param name="x_log"           type="boolean"                    _gui-text="Logarithmic X Subdiv. (Base given by entry above)">false</param>
      <param name="x_subsubdivs"    type="int"     min="1" max="1000" _gui-text="Subsubdivs. per X Subdivision:">1</param>
      <param name="x_half_freq"     type="int"     min="1" max="1000" _gui-text="Halve X Subsubdiv. Frequency after 'n' Subdivs. (log only):">1</param>
      <param name="x_divs_th"       type="float"   min="0" max="1000" _gui-text="Major X Division Thickness:">1</param>
      <param name="x_subdivs_th"    type="float"   min="0" max="1000" _gui-text="Minor X Division Thickness:">0.5</param>
      <param name="x_subsubdivs_th" type="float"   min="0" max="1000" _gui-text="Subminor X Division Thickness:">0.3</param>
      <param name="x_div_unit"      type="enum"                       _gui-text="X Division Thickness Unit:">
        <item value="px">px</item>
        <item value="mm">mm</item>
        <item value="cm">cm</item>
        <item value="in">in</item>
      </param>
    </page>
    <page name="y_tab" _gui-text="Y Axis">
      <param name="y_divs"          type="int"     min="1" max="1000" _gui-text="Major Y Divisions:">5</param>
      <param name="dy"              type="float"   min="1" max="1000" _gui-text="Major Y Division Spacing:">5</param>
      <param name="dy_unit"         type="enum"                       _gui-text="Major Y Division Spacing Unit:">
        <item value="mm">mm</item>
        <item value="cm">cm</item>
        <item value="in">in</item>
        <item value="px">px</item>
      </param>
      <param name="y_subdivs"       type="int"     min="1" max="1000" _gui-text="Subdivisions per Major Y Division:">1</param>
      <param name="y_log"           type="boolean"                    _gui-text="Logarithmic Y Subdiv. (Base given by entry above)">false</param>
      <param name="y_subsubdivs"    type="int"     min="1" max="1000" _gui-text="Subsubdivs. per Y Subdivision:">5</param>
      <param name="y_half_freq"     type="int"     min="1" max="1000" _gui-text="Halve Y Subsubdiv. Frequency after 'n' Subdivs. (log only):">1</param>
      <param name="y_divs_th"       type="float"   min="0" max="1000" _gui-text="Major Y Division Thickness:">1</param>
      <param name="y_subdivs_th"    type="float"   min="0" max="1000" _gui-text="Minor Y Division Thickness:">0.5</param>
      <param name="y_subsubdivs_th" type="float"   min="0" max="1000" _gui-text="Subminor Y Division Thickness:">0.3</param>
      <param name="y_div_unit"      type="enum"                       _gui-text="Y Division Thickness Unit:">
        <item value="px">px</item>
        <item value="mm">mm</item>
        <item value="cm">cm</item>
        <item value="in">in</item>
      </param>
    </page>
  </param>
    
  <effect>
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Render">
          <submenu name="Grids"/>
      </submenu>
    </effects-menu>
  </effect>
  <script>
    <command reldir="extensions" interpreter="python">grid_cartesian.py</command>
  </script>
</inkscape-extension>
