# coding=utf-8
from measure import Length
from inkex.tester import InkscapeExtensionTestMixin, TestCase

class LengthBasicTest(InkscapeExtensionTestMixin, TestCase):
    effect_class = Length
