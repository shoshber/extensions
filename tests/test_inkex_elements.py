#!/usr/bin/env python
# coding=utf-8
"""
Test elements extra logic from svg xml lxml custom classes.
"""

from lxml import etree

from inkex.elements import (
    BaseElement, ShapeElement, OtherElements,
    Group, Pattern, Guide, Polyline,
    TextElement, TextPath, FlowPara, FlowRoot, FlowRegion,
)
from inkex.transforms import Transform, ScaleTransform
from inkex.styles import Style
from inkex.tester import TestCase
from inkex.tester.svg import svg_file

class ElementTestCase(TestCase):
    """Base element test case"""
    tag = 'svg'

    def setUp(self):
        super(ElementTestCase, self).setUp()
        self.svg = svg_file(self.data_file('svg', 'complextransform.test.svg'))
        self.elem = self.svg.getElement('//svg:{}'.format(self.tag))

    def test_print(self):
        """Print element as string"""
        self.assertEqual(str(self.elem), self.tag)


class CoreElementTestCase(ElementTestCase):
    """Test core element functionality"""
    tag = 'g'

    def test_attr(self):
        """Access attributes"""
        elem = ShapeElement()
        self.assertRaises(AttributeError, getattr, elem, 'foo')
        self.assertRaises(NotImplementedError, elem.get_path)
        self.assertRaises(AttributeError, elem.set_path, 1)

    def test_findall(self):
        """Findall elements in svg"""
        groups = self.svg.findall('svg:g')
        self.assertEqual(len(groups), 1)

    def test_add(self):
        """Can add single or multiple elements with passthrough"""
        elem = self.svg.getElementById('D')
        group = elem.add(Group(id='foo'))
        self.assertEqual(group.get('id'), 'foo')
        groups = elem.add(Group(id='f1'), Group(id='f2'))
        self.assertEqual(len(groups), 2)
        self.assertEqual(groups[0].get('id'), 'f1')
        self.assertEqual(groups[1].get('id'), 'f2')

    def test_creation(self):
        """Create elements with attributes"""
        group = Group(attrib={'inkscape:label': 'Foo'})
        self.assertEqual(group.get('inkscape:label'), 'Foo')
        group = Group(inkscape__label="Bar")
        self.assertEqual(group.label, 'Bar')

    def test_sort_selected(self):
        """Are the selected items sorted"""
        self.svg.set_selected('G', 'B', 'D', 'F')
        self.assertEqual(tuple(self.svg.selected), ('G', 'B', 'D', 'F'))
        items = self.svg.get_z_selected()
        self.assertTrue(isinstance(items, dict))
        self.assertEqual(tuple(items), ('B', 'D', 'F', 'G'))

        self.svg.set_selected()
        self.assertEqual(tuple(self.svg.get_z_selected()), ())
        A_to_G = ('A', 'B', 'C', 'D', 'E', 'F', 'G')
        self.svg.set_selected(*A_to_G)
        self.assertEqual(tuple(self.svg.get_z_selected()), A_to_G)
        self.svg.set_selected('X', 'Y', 'Z', 'A')
        self.assertEqual(tuple(self.svg.get_z_selected()), ('A',))

    def test_transform(self):
        """In-place modified transforms are retained"""
        elem = self.svg.getElementById('D')
        self.assertEqual(str(elem.transform), 'translate(30, 10)')
        elem.transform.add_translate(-10, 10)
        self.assertEqual(str(elem.transform), 'translate(20, 20)')

    def test_scale(self):
        """In-place scaling from blank transform"""
        elem = self.svg.getElementById('F')
        self.assertEqual(elem.transform, Transform())
        self.assertEqual(elem.get('transform'), None)
        elem.transform.add_scale(1.0666666666666667, 1.0666666666666667)
        self.assertEqual(elem.get('transform'), ScaleTransform(1.06667))
        self.assertIn(b'transform', etree.tostring(elem))

    def test_in_place_transforms(self):
        """Do style and transforms update correctly"""
        elem = self.svg.getElementById('D')
        self.assertEqual(type(elem.transform), Transform)
        self.assertEqual(type(elem.style), Style)
        self.assertTrue(elem.transform)
        elem.transform = Transform()
        self.assertEqual(elem.transform, Transform())
        self.assertEqual(elem.get('transform'), None)
        self.assertNotIn(b'transform', etree.tostring(elem))
        elem.transform.add_translate(10, 10)
        self.assertIn(b'transform', etree.tostring(elem))
        elem.transform.add_translate(-10, -10)
        self.assertNotIn(b'transform', etree.tostring(elem))

    def test_update_consistant(self):
        """Update doesn't keep callbacks around"""
        elem = self.svg.getElementById('D')
        tr_a = Transform(translate=(10, 10))
        tr_b = Transform(translate=(-20, 15))
        elem.transform = tr_a
        elem.transform = tr_b
        self.assertEqual(str(elem.transform), 'translate(-20, 15)')
        tr_a.add_translate(10, 10)
        self.assertEqual(str(elem.transform), 'translate(-20, 15)')
        elem.set('transform', None)
        self.assertEqual(elem.get('transform'), None)

    def test_in_place_style(self):
        """Do styles update when we set them"""
        elem = self.svg.getElementById('D')
        elem.style['fill'] = 'purpleberry'
        self.assertEqual(elem.get('style'), 'fill:purpleberry')
        elem.style = Style(stroke='gammon')
        self.assertEqual(elem.get('style'), 'stroke:gammon')
        elem.style.update('grape:2;strawberry:nice;')
        self.assertEqual(elem.get('style'), 'stroke:gammon;grape:2;strawberry:nice')

    def test_random_id(self):
        """Test setting a random id"""
        elem = self.svg.getElementById('D')
        elem.set_random_id('Thing')
        self.assertEqual(elem.get('id'), 'Thing5815')
        elem.set_random_id('Thing', size=2)
        self.assertEqual(elem.get('id'), 'Thing85')

    def test_bounding_box(self):
        """Elements can have bounding boxes"""
        elem = self.svg.getElementById('D')
        self.assertEqual(elem.bounding_box(), (30.0, 70.0, 120.0, 160.0))
        self.assertEqual(elem.get_center_position(), (50.0, 140.0))
        self.assertFalse(TextElement().bounding_box())
        group = Group(elem)
        self.assertEqual(elem.bounding_box(), group.bounding_box())


    def test_path(self):
        """Test getting paths"""
        self.assertFalse(FlowRegion().get_path())
        self.assertFalse(FlowRoot().get_path())
        self.assertFalse(FlowPara().get_path())

    def test_descendants(self):
        """Elements can walk their descendants"""
        ids = tuple(elem.get('id') for elem in self.svg.descendants())
        self.assertEqual(ids, (
            None, None, 'path1', None,
            'base', 'metadata7',
             None, None, None, None, None,
            'A', 'B', 'C', 'D', 'E', 'F', 'G'
        ))

class PathElementTestCase(ElementTestCase):
    tag = 'path'

    def test_original_path(self):
        """LPE paths can return their original paths"""
        svg = svg_file(self.data_file('svg', 'with-lpe.svg'))
        lpe = svg.getElementById('lpe')
        nolpe = svg.getElementById('nolpe')
        self.assertEqual(str(lpe.path), 'M 30 30 L -10 -10 Z')
        self.assertEqual(str(lpe.original_path), 'M 20 20 L 10 10 Z')
        self.assertEqual(str(nolpe.path), 'M 30 30 L -10 -10 Z')
        self.assertEqual(str(nolpe.original_path), 'M 30 30 L -10 -10 Z')

        lpe.original_path = "M 60 60 L 5 5"
        self.assertEqual(lpe.get('inkscape:original-d'), 'M 60 60 L 5 5')
        self.assertEqual(lpe.get('d'), 'M 30 30 L -10 -10 Z')

        lpe.path = "M 60 60 L 15 15 Z"
        self.assertEqual(lpe.get('d'), 'M 60 60 L 15 15 Z')

        nolpe.original_path = "M 60 60 L 5 5"
        self.assertEqual(nolpe.get('inkscape:original-d', None), None)
        self.assertEqual(nolpe.get('d'), 'M 60 60 L 5 5')

class PolylineElementTestCase(TestCase):
    """Test the polyline elements support"""
    def test_polyline_points(self):
        """Basic tests for points attribute as a path"""
        pol = Polyline(points='10,10 50,50 10,15 15,10')
        self.assertEqual(str(pol.path), 'M 10 10 L 50 50 L 10 15 L 15 10')
        pol.path = "M 10 10 L 30 9 L 1 2 C 10 45 3 4 45 60 M 35 35"
        self.assertEqual(pol.get('points'), '10,10 30,9 1,2 45,60 35,35')

class PatternTestCase(ElementTestCase):
    tag = 'pattern'

    def test_pattern_transform(self):
        """Patterns have a transformation of their own"""
        pattern = Pattern()
        self.assertEqual(pattern.patternTransform, Transform())
        pattern.patternTransform.add_translate(10, 10)
        self.assertEqual(pattern.get('patternTransform'), 'translate(10, 10)')

class GroupTest(ElementTestCase):
    """Test extra functionality on a group element"""
    tag = 'g'

    def test_transform_property(self):
        """Test getting and setting a transform"""
        self.assertEqual(str(self.elem.transform), 'matrix(1.44985 0 0 1.36417 -107.03 -167.362)')
        self.elem.transform = 'translate(12, 14)'
        self.assertEqual(self.elem.transform, Transform('translate(12, 14)'))
        self.assertEqual(str(self.elem.transform), 'translate(12, 14)')

    def test_groupmode(self):
        """Get groupmode is layer"""
        self.assertEqual(self.svg.getElementById('A').groupmode, 'layer')
        self.assertEqual(self.svg.getElementById('C').groupmode, 'group')


class RectTest(ElementTestCase):
    """Test extra functionality on a rectangle element"""
    tag = 'rect'

    def test_compose_transform(self):
        """Composed transformation"""
        self.assertEqual(self.elem.transform, Transform('rotate(16.097889)'))
        self.assertEqual(str(self.elem.composed_transform()),
                         'matrix(0.754465 -0.863362 1.13818 1.31905 -461.592 215.193)')

    def test_compose_stylesheet(self):
        """Test finding the composed stylesheet for the shape"""
        self.assertEqual(str(self.elem.style), 'fill:#0000ff;stroke-width:1px')
        self.assertEqual(str(self.elem.composed_style()),
                         'fill:#0000ff;stroke:#d88;stroke-width:1px')

    def test_path(self):
        """Rectangle path"""
        self.assertEqual(self.elem.get_path(), 'M 200.0,200.0 h100.0v100.0h-100.0')
        self.assertEqual(str(self.elem.path), 'M 200 200 h 100 v 100 h -100')

class PathTest(ElementTestCase):
    """Test path extra functionality"""
    tag = 'path'

    def test_apply_transform(self):
        """Transformation can be applied to path"""
        path = self.svg.getElementById('D')
        path.transform = Transform(translate=(10, 10))
        self.assertEqual(path.get('d'), 'M30,130 L60,130 L60,120 L70,140 L60,160 L60,150 L30,150')
        path.apply_transform()
        self.assertEqual(path.get('d'), 'M 30 130 L 60 130 L 60 120 L 70 140 L 60 160 L 60 150 L 30 150')
        self.assertFalse(path.transform)

class CirtcleTest(ElementTestCase):
    """Test extra functionality on a circle element"""
    tag = 'circle'

    def test_path(self):
        """Circle path"""
        self.assertEqual(self.elem.get_path(),
                         'M 50.0 150.0 A 50.0,50.0 0 1 0 150.0, 100.0 A 50.0,50.0 0 1 0 50.0, 100.0')

class NamedViewTest(ElementTestCase):
    """Test the sodipodi namedview tag"""
    def test_guides(self):
        """Create a guide and see a list of them"""
        self.svg.namedview.add(Guide(0, 0, 0))
        self.svg.namedview.add(Guide(0, 0, 90))
        self.assertEqual(len(self.svg.namedview.get_guides()), 2)

class TextTest(ElementTestCase):
    """Test all text functions"""
    def test_append_superscript(self):
        """Test adding superscript"""
        tp = TextPath()
        tp.append_superscript('th')
        self.assertEqual(len(tp), 1)

class UseTest(ElementTestCase):
    """Test extra functionality on a use element"""
    tag = 'use'

    def test_path(self):
        """Use path follows ref"""
        self.assertEqual(str(self.elem.path), 'M 0 0 L 10 10 Z')
